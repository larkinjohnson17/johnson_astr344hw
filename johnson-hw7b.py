#ASTR344 HW7b: The Birthday Problem
#Larkin Johnson
#Because I missed the class on MC methods, I'm not 100% sure that I'm doing this in a traditional MC method
#My answer is 21 though you said the answer should be 23, I can't see what I'm doing that is causing me to be 2 off

import numpy as np

tot = 0
in_room = 2.0
prob = .5

while tot < prob:
    tot = tot + (in_room-1) / (365.25)
    in_room = in_room + 1

print "The smallest number of people needed in a room for the probability to be greater than 0.5 that two of them have the same birthday is:"
print in_room
        
