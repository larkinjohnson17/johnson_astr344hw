#  HW 2: Stability by Larkin Johnson

import numpy as np

#calculations for (1/3)^n up to n = 5

xnzero = np.float32(1)
xnone = np.float32(1./3)
i = 3


while i < 6:
    xntwo = (13./3) * xnone - (4./3) * xnzero
    xnzero = xnone
    xnone = np.float32 (xntwo)
    i = i +1
    
xmzero = np.float64(1)
xmone = np.float64(1./3)
j = 3

while j < 6:
    xmtwo = (13./3)*xmone-(4./3)*xmzero
    xmzero = xmone
    xmone = np.float64 (xmtwo)
    j = j + 1

absolute_error = xmone - xnone
relative_error = absolute_error / xmone

print "For (1/3)^n"
print "The abosolute error at n = 5 is"
print absolute_error
print "The relative error at n = 5 is"
print relative_error
print "-------------------------------------------------------------"

#calcuations for (1/3)^n up to n = 20

xnzero = np.float32(1)
xnone = np.float32(1./3)
i = 3

while i < 21:
    xntwo = (13./3) * xnone - (4./3) * xnzero
    xnzero = xnone
    xnone = np.float32 (xntwo)
    i = i +1
    
xmzero = np.float64(1)
xmone = np.float64(1./3)
j = 3

while j < 21:
    xmtwo = (13./3)*xmone-(4./3)*xmzero
    xmzero = xmone
    xmone = np.float64 (xmtwo)
    j = j + 1

absolute_error = xmone - xnone
relative_error = absolute_error / xmone

print "The abosolute error at n = 20 is"
print absolute_error
print "The relative error at n = 20 is"
print relative_error
print "-------------------------------------------------------------"

#calculations for 4^n for n = 20

xnzero = np.float32(1)
xnone = np.float32(4)
i = 3


while i < 21:
    xntwo = (13./3) * xnone - (4./3) * xnzero
    xnzero = xnone
    xnone = np.float32 (xntwo)
    i = i +1
    
xmzero = np.float64(1)
xmone = np.float64(4)
j = 3

while j < 21:
    xmtwo = (13./3)*xmone-(4./3)*xmzero
    xmzero = xmone
    xmone = np.float64 (xmtwo)
    j = j + 1

absolute_error = xmone - xnone
relative_error = absolute_error / xmone

print "For 4^n"
print "The abosolute error at n = 20 is"
print absolute_error
print "The relative error at n = 20 is"
print relative_error
print "4^n is a stable calculation because it has a very small relative error because 4^n increases as n increases"
print "Relative error is a good judge of accuracy and stability"
