#Project 1: Polio Mary
#Larkin Johnson

#Set up a 2 dimensional room with 100 placed at random x y locations

import numpy as np
import math
import matplotlib.pyplot as plt


def in_room_x(people):
    i = 0
    x_loc = np.zeros(people)
    for i in range(people):
        x = np.random.random()
        x_loc[i] = x
    return x_loc

def in_room_y(people):
    i = 0
    y_loc = np.zeros(people)
    for i in range(people):
        y = np.random.random()
        y_loc[i] = y
    return y_loc

#Move the people around the room

def sec_pass(x_loc, y_loc):
    i = 0
    for i in range(len(x_loc)):
        direc_x = np.random.random()
        if direc_x > .5:
            new_x = x_loc[i] + np.random.random() * .1
        else:
            new_x = x_loc[i] - np.random.random() * .1
        direc_y = np.random.random()
        if direc_y > .5:
            new_y = y_loc[i] + np.random.random() * .1
        else:
            new_y = y_loc[i] - np.random.random() * .1
        r = 0
        for r in range(len(x_loc)):
            if r == i:
                new_x = new_x
                new_y = new_y
            else:
                not_too_close(r, x_loc, y_loc, new_x, new_y)
        if new_x < 1:
            new_x = new_x
        else:
            new_x = np.random.random() 
        if new_y < 1:
            new_y = new_y
        else:
            new_y = np.random.random()
        if new_x > 0:
            new_x = new_x
        else:
            new_x = np.random.random()
        if new_y > 0:
            new_y = new_y
        else:
            new_y = np.random.random()
        x_loc[i] = new_x
        y_loc[i] = new_y

def not_too_close(r, x_loc, y_loc, new_x, new_y):
    dist = math.sqrt( (x_loc[r] - new_x)**2 + (y_loc[r] - new_y)**2)
    if dist < .025:
        direc_x = np.random.random()
        if direc_x >.5:
            new_x = new_x + .005
        else:
            new_x = new_x - .005
        direc_y = np.random.random()
        if direc_y > .5:
            new_y = new_y + .005
        else:
            new_y = new_y - .005
        not_too_close(r, x_loc, y_loc, new_x, new_y)
    else:
        new_x = new_x
        new_y = new_y

'''#Showing how people move: not actually needed for code to execute
x_pos = in_room_x(100)
y_pos = in_room_y(100)
x_orig = x_pos
y_orig = y_pos

plt.figure(1)
plt.subplot(211)
plt.plot(x_orig, y_orig, 'bo')

b = 0
for b in range(100):
    sec_pass(x_pos, y_pos)

plt.subplot(212)
plt.plot(x_pos, y_pos, 'ro')
plt.show()
#testing first half of code
test_x = in_room_x(10)
test_y = in_room_y(10)

print test_x
print test_y

sec_pass(test_x, test_y, 0)

print test_x
print test_y '''


#Make the infection matrix, 0 = uninfected, 1 = infected

def infect_mat(people):
    return np.zeros(people)


#Determining infection

def is_infected(x1, y1, x2, y2, person, distance, infection, vaccination):
    sum_of_squares = (x1 - x2)**2 + (y1-y2)**2
    dist = math.sqrt(sum_of_squares)
    if dist < distance:
        prob = np.random.random()
        if prob < .75:
            if vaccination[person] == 0:
                infection[person] = 1
            else:
                infection[person] = 0
        else:
            infection[person] = infection[person]
    else:
        infection[person] = infection[person]


def checking_infections(infection):
    sick_peeps = []
    i = 0
    for i in range(len(infection)):
        if infection[i] == 1:
            sick_peeps.append(i)
        else:
            sick_peeps = sick_peeps
    return sick_peeps

def make_sick(x, y, infection, rangee, vaccination):
    who_is_sick = checking_infections(infection)
    i = 0
    while(i < len(who_is_sick)):
        j = 0
        while(j < len(x)):
            k = who_is_sick[i] #returning the number of the person who is infected
            is_infected(x[k], y[k], x[j], y[j], j, rangee, infection, vaccination) #if j is close enough to infected person k, infect person j
            j = j+1
        i = i+1
    
#test how many people are uninfected
def num_uninfect(infection):
    i = 0
    count = 0
    for i in range(len(infection)):
        if infection[i] == 0:
            count = count + 1
        else:
            count = count
    return count

#make repetition of moving/checking infections

def spreading_polio(x, y, infection, rangee,  numinfected_matrix, time_matrix, vaccination, numvaccinated):
    make_sick(x, y, infection, rangee, vaccination)
    number_uninfected = num_uninfect(infection)
    number_infected = len(x) - number_uninfected
    if number_uninfected > numvaccinated + 1:
        time_matrix.append(time_matrix[-1] + 1)
        numinfected_matrix.append(number_infected)
        sec_pass(x, y)
        return spreading_polio(x, y, infection, rangee, numinfected_matrix, time_matrix, vaccination, numvaccinated)
    else:
        time_matrix.append(time_matrix[-1]+1)
        return time_matrix[-1]
        numinfected_matrix.append(len(x))
        print "The time it took to infected everyone in seconds was"
        print time_matrix[-1]
        print numinfected_matrix[0]
        plt.plot(time_matrix, numinfected_matrix, 'ro')
        plt.xlabel('Time [sec]')
        plt.ylabel('Number Infected')
        plt.title("Number of people infected as a function of time")
        plt.show()

#let's actually try this
#make the matrices

x_loc = in_room_x(100)
y_loc = in_room_y(100)
infections = infect_mat(100)
vaccination = np.zeros(100)

#infect 1 person
infections[0] = 1

#run the program with no one vaccinated
#spreading_polio(x_loc, y_loc, infections, .03, [1], [0], vaccination, 0)

#adding vaccinations

'''def spreading_polio_with_vaccinations(people, rangee, num_to_vac):
    i = 0
    vaccination = np.zeros(people)
    vac_matrix = []
    time_to_complete = []
    while i < num_to_vac:
        x_pos = in_room_x(people)
        y_pos = in_room_y(people)
        infec = infect_mat(people)
        infec[0] = 1
        vaccination[i+1] = 1
        time_it_took = spreading_polio(x_pos, y_pos, infec, rangee, [1], [0], vaccination, i)
        vac_matrix.append(i)
        time_to_complete.append(time_it_took[-1])
        i = i + 1
    print vac_matrix
    print time_to_complete
    plt.plot(vac_matrix, time_to_complete, 'ro')
    plt.xlabel('Number vaccinated')
    plt.ylabel('Time to infect all others')
    plt.title('Vaccinations vs. Time for Polio to Spread')
    plt.show()
    
        
spreading_polio_with_vaccinations(100, .03, 90)
        
#lets get an average of how long it takes'''

def all_the_times(number, total, vaccinated):
    times_to_average = np.zeros(number)
    vaccination = np.zeros(total)
    j = 0
    for j in range(vaccinated):
        vaccination[j+1] = 1
    i = 0
    for i in range(number):
        x_pos = in_room_x(total)
        y_pos = in_room_y(total)
        infec = infect_mat(total)
        infec[0] = 1
        times_to_average[i] = spreading_polio(x_pos, y_pos, infec, .03, [1], [0], vaccination, vaccinated)
    print times_to_average
    sum = 0
    k = 0
    for k in range(len(times_to_average)):
        sum = sum + times_to_average[k]
    return (sum * 1.0) / len(times_to_average)
        
print all_the_times(50, 100, 0)
    
    


        
    
