#Homework 4: How Big are Objects in the Universe?
#Larkin Johnson, ASTR344, 09/29/15

import numpy as np
import matplotlib.pyplot as plt

def trap_rule_int(start, end, spaces, f_of_x):
    spacing = (end - start)/spaces
    x_values = np.linspace(start, end, num=spaces)
    i = 0
    integral = 0
    for i in range(len(x_values)-1):
         add = .5*(spacing) * (f_of_x(x_values[i]) + f_of_x(x_values[i+1]))
         integral = integral + add
    return integral

def r_of_x(x):
    result = (3 * 10 ** 3)/((.3*(1 + x)**3 + .7)** 0.5)
    return result

def DA(z, spaces):
    D_sub_A = trap_rule_int(0, z, spaces, r_of_x) / (1 + z)
    return D_sub_A


z_values = np.linspace(0, 5, num=100) # Each individual Z value that we are evaluating
spacing = 5./100

size = len(z_values)
distance = []
distance_2 = []

for j in range(size):
	distance.append(DA(z_values[j], size))

for k in range(size):
    x = np.linspace(0, z_values[k], num = size)
    r = map(r_of_x, x)
    new_value = np.trapz(r, x) / (1 + z_values[k])
    distance_2.append(new_value)
print distance_2

plt.plot(z_values, distance, 'bs', z_values, distance_2, 'rs')
plt.xlabel('Redshifts')
plt.ylabel('Angular Diameter Distance')
plt.show()
