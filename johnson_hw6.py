#Larkin Johnson Astr 344 HW6
# Oct. 22 2015

#Part 1: Root Finding of Arbritary Functions

import numpy as np
import math

from datetime import datetime
from datetime import timedelta

tol = 0.001

#set functions
def f(x):
    return np.sin(x)
def g(x):
    return (x**3)-x-2
def y(x):
    return -6 + x + (x**2)

#set derivatives
def df(x):
    return np.cos(x)
def dg(x):
    return 3*(x**2)-1
def dy(x):
    return 1+2*x

#use NR method
def NR(xi, r, dr):
    val = r(xi)
    der = dr (xi)
    return xi - val / (der*1.0) 


def find_root_NR(guess, r, dr):
    if abs(r(guess)) >= tol:
        new_guess = NR(guess, r, dr)
        return find_root_NR(new_guess, r, dr)
    else:
        return guess

f_guess = 2
g_guess = 1
y_guess = 0
    
#evaluate for given functions
t1 = datetime.now()
print "The root of f(x) is:"
print find_root_NR(f_guess, f, df)
print "The root of g(x) is:"
print find_root_NR(g_guess, g, dg)
print "The root of y(x) is:"
print find_root_NR(y_guess, y, dy)
t2 = datetime.now()
tdelta = t2 - t1
seconds = tdelta.total_seconds()

print "The time it took to run this code (in seconds)"
print seconds
print "The time it takes the bisection method to run is 0.00031 seconds"

difference = .00031 - seconds
print "The NR method is faster by:"
print difference
print "seconds"

#part 2: Finding the Temperature of a Black Body
#constants
c = 2.99792458 * 10**10
h = 6.6260755 * 10**(-27)
k = 1.380658 * 10**(-16)

#given
wavelength = 870 * 10**(-4) #cm
B = 1.25 * 10**(-12)
freq = c / (wavelength)

def function(x):
    return ((2*h*freq**3)/(c**2))/(math.exp((h*freq)/(k*x))-1) - B
def deriv(x):
    return ( 2 * math.exp((h*freq)/(k*x)) * (h**2) * freq**4)/( (c**2) *( (math.exp((h*freq)/(k*x)) - 1 ) **2) * k * x**2)

tol = 1 * 10**(-16)
print "The temperature of the Black Body is:"
print find_root_NR(20, function, deriv)

