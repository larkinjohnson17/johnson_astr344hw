#Homework 3--Differentials of the Number of Early Universe Starbursts
#Larkin Johnson
#Due 09/22/15

import matplotlib.pyplot as plt
import numpy as np

#Import data and givens
model = np.loadtxt('model_smg.dat')
L_model = model[:,0]
L_model_adjusted = [L_model[1], L_model[2], L_model[3], L_model[4], L_model[5], L_model[6], L_model[7], L_model[8], L_model[9], L_model[10]]
gal_model= model[:,1]
data = np.loadtxt('ncounts_850.dat')
log_L_data = data[:,0]
L_data = []
dNdL_data = data[:,1]
dNdL_model=[]

i = 1

k = 0

#Convert model to L vs. dN / dL
for i in range(10):
    h1 = L_model[i]-L_model[i-1]
    h2 = L_model[i+1]-L_model[i]
    dNdL_model.append((h1/(h2*(h1+h2)))*gal_model[i+1]-((h1-h2)/(h1*h2))*gal_model[i]-(h2/(h1*(h1+h2)))*gal_model[i-1])
#Convert data to L vs. dN / dL (out of log form)
for k in range(154):
    point = log_L_data[k]
    L_data.append( 10 ** (point) )
#Plot 
plt.plot(L_model_adjusted, dNdL_model, 'rs', L_data, dNdL_data, 'bs')
plt.ylabel('dN/dL')
plt.xlabel('L')
plt.title('Derivative of number of galaxies above given luminosity vs. Luminosity')
plt.show()

 
