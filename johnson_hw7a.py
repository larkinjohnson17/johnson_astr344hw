#Calculating pi, Larkin Johnson, Oct 29

import numpy as np

def func(N1):
    i = 0
    N1_in_circle = 0
    for i in range(N1):
        x = 2*np.random.random() - 1
        y = 2*np.random.random()- 1
        if (x**2 + y**2) < 1:
            N1_in_circle = N1_in_circle + 1
        else:
            N1_in_circle = N1_in_circle
    return (N1_in_circle*4)/(N1*1.0)

print "When N = 10000 our function predicts pi will be"
print func(10000)
print "When N = 100000 our function predicts pi will be"
print func(100000)
print "When N = 1000000 our function predicts pi will be"
print func(1000000)
print "When N = 10000000 our function predicts pi will be"
print func(10000000)
print "Using my method, it requires a sample size of 1,000,000 to get our prediction for pi to 3.141 +- .001"



