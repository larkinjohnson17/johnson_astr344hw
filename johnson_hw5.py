#Rootfinding of Arbritary Functions
#Larkin Johnson, HW5, due Oct. 8

import numpy as np
import pdb
from datetime import datetime
from datetime import timedelta 

#set functions
def f(x):
    return np.sin(x)
def g(x):
    return (x**3)-x-2
def y(x):
    return -6 + x + (x**2)

#set tolerance, max number of iterations
it_max = 100
tolerance = 0.001

#create function to find root
def find_root(r, x1, x2):
    iterations = 0
    if r(x1) / r(x2) <= 0:
        avg = (x1 + x2) / 2.0
        if abs(r(avg)) <= tolerance:
            return avg
        else: 
            if r(avg) / r(x2) <= 0:
                return find_root(r, avg, x2)
            else:
                return find_root(r, x1, avg)
    else:
        if iterations < it_max / 2.0 :
            new_x1 = 2 * x1
            iterations = iterations + 1
            return find_root(r, new_x1, x2)
        elif iterations < it_max:
            new_x2 = 2 * x2
            iterations = iterations + 1
            return find_root(r, x1, new_x2)
        else:
            return "The root could not be found"

t1 = datetime.now()
#Evaluate for given functions
print "The root of f(x) is:"
print find_root(f, 2, 4)
print "The root of g(x) is:"
print find_root(g, 1, 2)
print "The root of y(x) is:"
print find_root(y, 0, 5)
t2 = datetime.now()
tdelta = t2 - t1
seconds = tdelta.total_seconds()

print seconds

    
        
    
